# Geezerpin iOS-dev challenages

## Properties List - Small iOS app challenge

In this challenge, you need to create a native iOS app that lists properties for an online property rental website. Other than installing it on Android phones or tablets, the Android app would possibly be embedded in the public display devices on the streets or on the buses for advertisements.

To show case your iOS app development skills, you are asked to implement the minimum of the iOS application. However, it is not required to look exactly the same as what you can see on the wireframe.

You are expected to develop the app natively using Swift.

The iOS app that you create needs to be responsive. That is, it supports iOS phones or tablets in different screen sizes. Provided in the `properties-list` folder, `wireframe-desktop.png` is the wireframe for desktop, the mobile version is up to your creativity.

In the `properties-list` folder, you can also find the API document. You will fetch the properties data throught the API calls. The API endpoint is `https://api.dwellease.co`.
